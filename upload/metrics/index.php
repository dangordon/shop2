<?php

header("Content-Type: text/plain");

$orders = mt_rand() / mt_getrandmax() * 20 + 200;
$order_value_avg = mt_rand() / mt_getrandmax() * 100 + 300;
$order_suggested_items = mt_rand() / mt_getrandmax() * 1 + 0;

echo "metric_order_count $orders \n";
echo "metric_order_value $order_value_avg \n";
echo "metric_order_suggested $order_suggested_items \n";